import cv2
import pdb
from xml_decoder import xml_file_decoder
import textwrap
import os

def process_new_video(xml_path, video_path, target_video_path, time_bias):
    cap = cv2.VideoCapture(video_path)
    fps = cap.get(cv2.CAP_PROP_FPS)
    decoder = xml_file_decoder(xml_path, time_bias)
    decoded_list, feature_list = decoder.decode_xml_file()
    decode_list_index = 0
    current_step = [0.0, "None"]
    frame_number_counter = 0
    step_color = (255,165,0)
    # We need to set resolutions.
    # so, convert them from float to integer.
    frame_width = int(cap.get(3))
    frame_height = int(cap.get(4))

    size = (int(frame_width * 0.6), int(frame_height * 0.6))
    # Below VideoWriter object will create
    # a frame of above defined The output
    # is stored in 'filename.avi' file.
    result = cv2.VideoWriter(target_video_path,
                             cv2.VideoWriter_fourcc(*'MJPG'),
                             fps, size)
    print("Generating video, please wait...")
    while(1):
        ret, frame = cap.read()
        frame_number_counter += 1
        if cv2.waitKey(int(1000 / fps)) & 0xFF == ord('q') or ret == False:
            cap.release()
            cv2.destroyAllWindows()
            break
        frame = cv2.resize(frame, None, None, fx=0.6, fy=0.6)
        # pdb.set_trace()
        previous_time = (frame_number_counter-1) / fps
        current_time = frame_number_counter / fps
        if decoded_list[decode_list_index]['EndTime'] < current_time and decode_list_index < len(decoded_list)-1:
            print(f"Finish segment: {decode_list_index}")
            decode_list_index += 1

        for sig_step in decoded_list[decode_list_index]['step_time_list']:
            if sig_step[0] >= previous_time and sig_step[0] <= current_time:
                'update current step when new step is available'
                current_step = sig_step
                # 'switch the color'
                # if step_color == (255,165,0):
                #     step_color = (30,144,255)
                # else:
                #     step_color = (255,165,0)

        feature_string = f"\nStart time: NAN \n" \
                         f"End time: NAN \n" \
                         f"Description: NAN"
        for single_feature in feature_list:
            if current_time <= single_feature['Feature End time'] and current_time >= single_feature['Feature Start time']:
                feature_string = f"\nStart time: {round(single_feature['Feature Start time'], 4)} \n" \
                                 f"End time: {round(single_feature['Feature End time'], 4)} \n" \
                                 f"Description: {textwrap.fill(single_feature['Description'], 20)}"

        information_string = f"Time: {round(current_time, 4)} \n"\
                             f"PathSegmentID: {decoded_list[decode_list_index]['PathSegmentID']} SpatialSegmentID: {decoded_list[decode_list_index]['SpatialSegmentID']} \n" \
                             f"Direction: {decoded_list[decode_list_index]['Direction']} NSteps: {decoded_list[decode_list_index]['NSteps']} \n" \
                             f"Step recorded time: {round(current_step[0], 4)}, foot: {current_step[1]}"

        step_information = f"foot: {current_step[1]}"

        position = (10, 50)
        text = information_string + feature_string
        font_scale = 0.75
        color_list = [(255, 255, 255), (255, 255, 255), step_color, step_color, (255,215,0), (255,215,0)] + [(127,255,0)] * (len(feature_string.split("\n"))-2)
        thickness = 2
        font = cv2.FONT_HERSHEY_SIMPLEX

        text_size, _ = cv2.getTextSize(text, font, font_scale, thickness)
        line_height = text_size[1] + 5
        x, y0 = position
        for i, line in enumerate(text.split("\n")):
            y = y0 + i * line_height
            cv2.putText(frame,
                        line,
                        (x, y),
                        font,
                        font_scale,
                        color_list[i],
                        thickness)

        cv2.putText(frame,
                    step_information,
                    (576, 820),
                    font,
                    font_scale,
                    step_color,
                    thickness)
        result.write(frame)
        # cv2.imshow('frame', frame)

    # When everything done, release
    # the video capture and video
    # write objects
    cap.release()
    result.release()

    # Closes all the frames
    cv2.destroyAllWindows()

    print("The video was successfully saved")

def create_folders(directory):
    if not os.path.exists(directory):
        os.makedirs(directory)

if __name__ == '__main__':
    time_bias_series = [
        [9.2884-3.5, 7.8918-3.7, 6.4409-13.8, 7.8015-10.9, 9.1488-11.4, 6.6677-7.9],
        [6.2581-8.25, 7.4661-18.6, 4.7494-16.9, 8.0686-8.15, 6.6253 - 11.67, 7.0733 - 11.4],
        [8.0863-5.83, 7.7779-5.6, 8.1619-4.47, 6.6284 - 5.57, 6.5700-5.77, 4.7806-3.67],
        [4.7710-5.7, 3.9468-4.7, 4.9008-6.5, 4.5457-7.54, 4.6139-8.6, 4.8820-5.27],
        [3.9987-3.29, 3.4078-6.64, 5.9549-9.7, 3.7830-6.2, 4.1409-6.64, 5.6687-9.8],
        [9.1119-10.9, 3.4076-3.73, 3.9519-7.04, 3.5832 - 7.4, 2.9567-5.9, 4.6670 - 10.8],
        [4.3565-10.4, 3.8359-8.6, 3.3801-6.25, 3.1409-5.74, 4.6489-9.9, 3.1030-5.15],
        [3.4766 - 2.41, 5.1071-1.0, 3.5099-1.0, 6.9626-1.15, 3.7621-3.97, 4.3595-2.63],
        [3.4851 - 4.75, 3.1101-5.4, 2.6649-1.58, 2.4564-2.50, 3.4954-1.50, 2.2617-2.26]
    ]

    correspond_dict = {
        0: 13,
        1: 14,
        2: 15,
        3: 1,
        4: 2,
        5: 3,
        6: 4,
        7: 5,
        8: 6,
        10: 12
    }

    for time_data_index, id_index in enumerate([0, 1, 2, 4, 5, 6, 7, 8, 10]):
        # if id_index in [0, 1, 2, 4, 5, 7, 8, 10]:
        #     continue
        root_video_path = f"/Users/renpeng/Downloads/videos/Videos/Participant_ID{id_index}/"
        time_bias = time_bias_series[time_data_index]
        corresponding_weallwalk_id = correspond_dict[id_index]
        assistant = "WC" if corresponding_weallwalk_id <= 10 else "NA"
        assistant = "GD" if corresponding_weallwalk_id == 4 else assistant
        target_video_path = f"/Users/renpeng/PycharmProjects/video_search/video/ID{corresponding_weallwalk_id}"
        create_folders(target_video_path)
        for i in range(1, 7):
            process_new_video(xml_path=f"/Users/renpeng/Downloads/weallwalk/xml/T{i}_ID{corresponding_weallwalk_id}_{assistant}.xml",
                              video_path=f"{root_video_path}Trial{i}.mp4",
                              target_video_path=f"{target_video_path}/T{i}_ID{corresponding_weallwalk_id}_{assistant}.avi", time_bias=time_bias[i - 1])

    time_bias = [4.4, 2.0, 5.6565-6.1, 5.8355-5.87, 5.5238-4.0, 5.8663-6.13]
    for i in range(1, 7):
        target_video_path = f"/Users/renpeng/PycharmProjects/video_search/video/ID1/"
        create_folders(target_video_path)
        target_video_path = f"/Users/renpeng/PycharmProjects/video_search/video/ID1/" + "WhiteCane/"
        create_folders(target_video_path)
        process_new_video(xml_path=f"/Users/renpeng/Downloads/weallwalk/xml/T{i}_ID1_WC.xml",
                          video_path= f"/Users/renpeng/Downloads/videos/Videos/Participant_ID3/WhiteCane/Trial{i}.mp4",
                          target_video_path=f"{target_video_path}/T{i}_ID1_WC.avi", time_bias=time_bias[i-1])

    time_bias = [4.2710-4.4, 5.7692-7.50, 3.9504-3.57, 4.3502 - 3.9, 4.0601-2.3, 6.1241-9.27]
    for i in range(1, 7):
        if i==5:
            continue
        target_video_path = f"/Users/renpeng/PycharmProjects/video_search/video/ID1/"
        create_folders(target_video_path)
        target_video_path = f"/Users/renpeng/PycharmProjects/video_search/video/ID1/" + "GuideDog/"
        create_folders(target_video_path)
        process_new_video(xml_path=f"/Users/renpeng/Downloads/weallwalk/xml/T{i}_ID1_GD.xml",
                          video_path=f"/Users/renpeng/Downloads/videos/Videos/Participant_ID3/GuideDog/Trial{i}.mp4",
                          target_video_path=f"{target_video_path}/T{i}_ID1_GD.avi", time_bias=time_bias[i - 1])