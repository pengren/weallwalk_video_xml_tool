from benedict import benedict as bdict
from pprint import pprint
import pdb

class xml_file_decoder(object):
    def __init__(self, xml_path, time_bias):
        self.xml_data = bdict.from_xml(xml_path)['root']['Segments']['PathSegment']
        print(f"Fourth step time: {self.xml_data[0]['Steps']['Step'][0]['Time']}")
        # self.time_bias = float(self.xml_data[0]['StartTime'])
        self.time_bias = time_bias

    def decode_xml_file(self):
        decoded_list = []
        feature_list = []
        for single_chunk_data in self.xml_data:
            'build step time list'
            step_time_list = []

            if isinstance(single_chunk_data['Steps']['Step'], dict):
                step_time_list.append([float(single_chunk_data['Steps']['Step']['Time']) - self.time_bias, single_chunk_data['Steps']['Step']['WhichFoot']])
            else:
                for sig_step in single_chunk_data['Steps']['Step']:
                    step_time_list.append([float(sig_step['Time']) - self.time_bias, sig_step['WhichFoot']])

            info_dict = {
                'PathSegmentID': single_chunk_data['PathSegmentID'],
                'SpatialSegmentID': single_chunk_data['SpatialSegmentID'],
                'NSteps': single_chunk_data['NSteps'],
                'Direction': single_chunk_data['Direction'],
                'StartTime': float(single_chunk_data['StartTime']) - self.time_bias,
                'EndTime': float(single_chunk_data['EndTime']) - self.time_bias,
                'step_time_list': step_time_list
            }
            # pprint(info_dict)
            decoded_list.append(info_dict)

            if "Features" in single_chunk_data:
                if isinstance(single_chunk_data['Features']['Feature'], dict):
                    Description = single_chunk_data['Features']['Feature']['Description']
                    start_time = float(single_chunk_data['Features']['Feature']['StartTime']) - self.time_bias
                    end_time = float(single_chunk_data['Features']['Feature']['EndTime']) - self.time_bias
                    # pprint(
                    #     {
                    #         'Feature Start time':start_time,
                    #         'Feature End time': end_time,
                    #         'Description': Description
                    #     }
                    # )
                    feature_list.append(
                        {
                            'Feature Start time': start_time,
                            'Feature End time': end_time,
                            'Description': Description
                        }
                    )
                else:
                    for sig_interval in single_chunk_data['Features']['Feature']:
                        Description = sig_interval['Description']
                        start_time = float(sig_interval['StartTime']) - self.time_bias
                        end_time = float(sig_interval['EndTime']) - self.time_bias
                        # pprint(
                        #     {
                        #         'Feature Start time': start_time,
                        #         'Feature End time': end_time,
                        #         'Description': Description
                        #     }
                        # )
                        feature_list.append(
                            {
                                'Feature Start time': start_time,
                                'Feature End time': end_time,
                                'Description': Description
                            }
                        )
        print("XML readin")
        return decoded_list, feature_list

if __name__ == '__main__':
    decoder = xml_file_decoder("T1_ID1_WC.xml")
    decoder.decode_xml_file()